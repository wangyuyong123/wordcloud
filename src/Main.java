import java.util.List;

public class Main {

    public static ReadFileCls read;

    public static WordCount wordCount;

    public static void main(String[] args) {
        if (args.length >= 3) {
            int i = 0;
            if (args[i].equals("-a") || args[++i].equals("-a")) {
                int j = 1 - i;
                if (args[j].equals("-c")) {
                    try {
                        operateAllFile(args[2], new Do() {
                            @Override
                            public void doSomething(String fileName, WordCount wordCount) {
                                System.out.println(fileName + "的字符数为：" + wordCount.getContentLength());
                            }
                        });
                    }catch (Exception e) {
                        showNoFile();
                    }
                }else if (args[j].equals("-w")) {
                    try {
                        operateAllFile(args[2], new Do() {
                            @Override
                            public void doSomething(String fileName, WordCount wordCount) {
                                System.out.println(fileName + "的单词数为：" + wordCount.getWordCount());
                            }
                        });
                    }catch (Exception e) {
                        showNoFile();
                    }
                }else if (args[j].equals("-l")) {
                    try {
                        operateAllFile(args[2], new Do() {
                            @Override
                            public void doSomething(String fileName, WordCount wordCount) {
                                System.out.println(fileName + "的行数为：" + wordCount.getContentLine());
                            }
                        });
                    }catch (Exception e) {
                        showNoFile();
                    }
                }else {
                    showError();
                }
            }else {
                showError();
            }
        } else if (args.length >= 2) {

            if (args[0].equals("-c")) {
                try {
                    init(args[1]);
                    System.out.println(args[1] + "字符数为：" + wordCount.getContentLength());
                }catch (Exception e) {
                    showNoFile();
                }
            }else if (args[0].equals("-w")) {
                try {
                    init(args[1]);
                    System.out.println(args[1] + "单词数：" + wordCount.getWordCount());
                }catch (Exception e) {
                    showNoFile();
                }

            }else if (args[0].equals("-l")) {
                try {
                    init(args[1]);
                    System.out.println(args[1] + "行数：" + wordCount.getContentLine());
                }catch (Exception e) {
                    showNoFile();
                }

            }else {
                showError();
            }
        } else {
            showError();
        }
    }

    public static void init(String filePath) throws Exception{
        read = new ReadFileCls(filePath);
        wordCount = new WordCount(read.getContent());
    }

    public static void showError() {
        System.out.println("参数不规范");
    }

    public static void showNoFile() {
        System.out.println("没有找到该文件");
    }

    public static void operateAllFile(String directory, Do operator)throws Exception{
        read = new ReadFileCls(directory,true);
        List<String> fileContents = read.getAllContent();
        List<String> fileName = read.getAllFileName();
        for (int i = 0; i < fileContents.size(); i++) {
            wordCount = new WordCount(fileContents.get(i));
            if (operator != null) {
                operator.doSomething(fileName.get(i),wordCount);
            }
        }
    }
}
