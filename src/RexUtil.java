import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 正则表达式处理类
 */
public class RexUtil {

    Pattern pattern;
    Matcher matcher;

    public RexUtil(String rex, String content) {
        pattern = Pattern.compile(rex);
        matcher = pattern.matcher(content);
    }

    /**
     * 返回捕获组
     * @return 匹配的字符串
     */
    public List<String> getRexString() {
        List<String> note = new ArrayList<>();
        while (matcher.find()) {
            note.add(matcher.group(1));
        }
        return note;
        /*if (matcher.matches()) {
            List<String> note = new ArrayList<>();
            int maxGroup = matcher.groupCount();
            for (int i = 1; i <= maxGroup; i++) {
                note.add(matcher.group(i));
            }
            return note;
        }else {
            return null;
        }*/
    }
}
