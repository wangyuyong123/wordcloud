import java.util.List;

/**
 * 获取字符串的各种信息
 */
public class WordCount {

    private String content;

    /**
     * 所有行
     */
    private int allLine = -1;

    /**
     * 空格
     */
    private int space = -1;

    /**
     * 空行
     */
    private int spaceLine = -1;

    /**
     * 代码行
     */
    private int codeLine = -1;

    /**
     * 注释行
     */
    private int NoteLine = -1;

    private static final String ILLEGAL_CHARACTERS_REX = "[^a-zA-Z]";

    private static final String BLANK_REX = " +";

    private static final String NOTE_REX = "((//.*\\n)|(/\\*[.\\n]*\\*/))";

    public WordCount(String content) {
        this.content = content;
    }

    /**
     * 获取字符串字符的数量
     * @return 数量
     */
    public int getContentLength() {
        if (content != null) {
            return content.length();
        }else {
            return 0;
        }
    }

    /**
     * 获取内容的行数
     * @return 行数
     */
    public int getContentLine() {
        if (allLine == -1) {
            if (content != null) {
                String[] lines = content.split("\n");
                if (lines.length == 1 && lines[0].equals("")) {
                    allLine = 0;
                    return allLine;
                }
                allLine = lines.length;
                return allLine;
            } else {
                allLine = 0;
                return allLine;
            }
        }else {
            return allLine;
        }
    }

    /**
     * 获取空行
     * @return 空行数量
     */
    public int getSpaceLine() {
        if (spaceLine == -1) {
            if (content != null) {
                int num = 0;
                String[] fileLinse = content.split("\n");
                for (int i = 0; i < fileLinse.length; i++) {
                    if (fileLinse[i].equals("")) {
                        num++;
                    }
                }
                spaceLine = num;
                return spaceLine;
            }else {
                spaceLine = 0;
                return spaceLine;
            }
        }else {
            return spaceLine;
        }
    }

    /**
     * 获取注释行
     * @return 注释行行数
     */
    public int getNoteLine() {
        List<String> notes = new RexUtil(NOTE_REX,content).getRexString();
        int lines = 0;
        for (int i = 0; i < notes.size(); i++) {
            String note = notes.get(i);
            System.out.println(note);
            if (note != null) {
                String[] tip = note.split("\n");
                lines += tip.length;
            }
        }
        return lines;
    }

    /**
     * 获取单词数目
     * @return 单词数目
     */
    public int getWordCount() {
        //用正则表达式去除非法字符
        if (content != null) {
            String temp = content.replaceAll(ILLEGAL_CHARACTERS_REX, " ");
            String[] words = temp.split(BLANK_REX);
            if (words.length == 1 && words[0].equals("")) {
                return 0;
            }
            return words.length - 1;
        }else {
            return 0;
        }
    }
}
