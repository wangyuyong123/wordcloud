import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadFileCls {

    private File file;

    private boolean isAll;

    private String content;

    public ReadFileCls(String filePath) {
        this.file = new File(filePath);
    }

    public ReadFileCls(String filePath, boolean isAll) {
        this.file = new File(filePath);
        this.isAll = isAll;
    }

    /**
     * 如果是文件夹，则递归获取文件夹内的所有内容
     * 如果是文件，则获取该文件的内容
     * @return 字符串数组
     * @throws Exception 若该文件不存在，则抛出异常。
     */
    public List<String> getAllContent() throws Exception{
        List<String> allContent = new ArrayList<>();
        if (isAll) {
            if (file.exists()) {
                if (file.isFile()) {
                    allContent.add(getContent());
                }else {
                    File[] fileSon = file.listFiles();
                    if (fileSon != null) {
                        for (int i = 0; i < fileSon.length; i++) {
                            List<String> temp = new ReadFileCls(fileSon[i].getAbsolutePath(), true).getAllContent();
                            allContent.addAll(temp);
                        }
                    }
                }
            }else {
                throw new FileNotFoundException("没有此文件");
            }
        }else {
            allContent.add(getContent());
        }

        return allContent;
    }

    /**
     * 如果是文件夹，则递归获取文件夹内的文件的名字
     * 如果是文件，则获取该文件的名字
     * @return 字符串数组
     * @throws Exception 若该文件不存在，则抛出异常。
     */
    public List<String> getAllFileName() throws Exception{
        List<String> allFileName = new ArrayList<>();
        if (isAll) {
            if (file.exists()) {
                if (file.isFile()) {
                    allFileName.add(file.getName());
                }else {
                    File[] fileSon = file.listFiles();
                    if (fileSon != null) {
                        for (int i = 0; i < fileSon.length; i++) {
                            List<String> temp = new ReadFileCls(fileSon[i].getAbsolutePath(), true).getAllFileName();
                            allFileName.addAll(temp);
                        }
                    }
                }
            }else {
                throw new FileNotFoundException("没有此文件");
            }
        }else {
            allFileName.add(file.getName());
        }

        return allFileName;
    }

    /**
     * 获取文件内容
     * @return String 文件内容(若返回值为null,说明文件中没有内容)
     * @throws Exception 若该文件不存在，则抛出异常
     */
    public String getContent() throws Exception{

        BufferedReader reader = null;
        StringBuilder builder = new StringBuilder();

        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
            String temp = null;
            while ((temp = reader.readLine()) != null) {
                builder.append(temp + "\n");
            }
            content = builder.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } finally {
            if (reader != null) {
                reader.close();
            }
        }
        return content;
    }
}
