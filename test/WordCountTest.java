import junit.framework.TestCase;

public class WordCountTest extends TestCase {

    private ReadFileCls read;
    private WordCount wordCount;

    {
        read = new ReadFileCls("C:\\IdeaProjects\\WordClount\\test\\test.c");
        try {
            wordCount = new WordCount(read.getContent());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testGetContentLength() {
        System.out.println("test.c的字符总数为"+wordCount.getContentLength());
    }

    public void testGetContentLine() {
        System.out.println("test.c的字符行数为"+wordCount.getContentLine());
    }

    public void testGetSpaceLine() {
        System.out.println("test.c的空行数为"+wordCount.getSpaceLine());
    }

    public void testGetNoteLine() {
        System.out.println("test.c的注释行数为"+wordCount.getNoteLine());
    }

    public void testGetWordCount() {
        System.out.println("test.c的单词数目为"+wordCount.getWordCount());
    }
}