import junit.framework.TestCase;

import java.util.List;

public class ReadFileClsTest extends TestCase {

    /**
     * 测试ReadFileCls能否正确获得字符串
     */
    public void testGetContent() {
        ReadFileCls test = new ReadFileCls("C:\\IdeaProjects\\WordClount\\test\\test.c");
        try {
            String content = test.getContent();
            if (content == null) {
                System.out.println("文件里没有内容");
            }else {
                System.out.println(content);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("没有该文件");
        }
    }

    /**
     * 测试wordcount模块
     */
    public void testWordCount() {
        ReadFileCls file1 = new ReadFileCls("C:\\IdeaProjects\\WordClount\\test\\test.c");
        ReadFileCls file2 = new ReadFileCls("C:\\IdeaProjects\\WordClount\\test\\text02.c");
        ReadFileCls file3 = new ReadFileCls("C:\\IdeaProjects\\WordClount\\test\\texto3.c");

        try {
            String conten1 = file1.getContent();
            String conten2 = file2.getContent();
            String content3 = file3.getContent();
            WordCount wordCount1 = new WordCount(conten1);
            WordCount wordCount2 = new WordCount(conten2);
            WordCount wordCount3 = new WordCount(content3);
            System.out.println("test.c字符数：" + wordCount1.getContentLength());
            System.out.println("test.c行数：" + wordCount1.getContentLine());
            System.out.println("test.c单词数：" + wordCount1.getWordCount());
            System.out.println("test02.c字符数：" + wordCount2.getContentLength());
            System.out.println("test02.c行数：" + wordCount2.getContentLine());
            System.out.println("test02.c单词数：" + wordCount2.getWordCount());
            System.out.println("test03.c字符数：" + wordCount3.getContentLength());
            System.out.println("test03.c行数：" + wordCount3.getContentLine());
            System.out.println("test03.c单词数：" + wordCount3.getWordCount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void testGetAllContent() {
        ReadFileCls read = new ReadFileCls("C:\\test01",true);
        try {
            List<String> allContent = read.getAllContent();
            for (int i = 0; i < allContent.size(); i++) {
                System.out.println(allContent.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("没有该文件");
        }
    }

    public void testGetAllFileName() {
        ReadFileCls read = new ReadFileCls("C:\\test01",true);
        try {
            List<String> allFileName = read.getAllFileName();
            for (int i = 0; i < allFileName.size(); i++) {
                System.out.println(allFileName.get(i));
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("没有该文件");
        }
    }


}