import junit.framework.TestCase;

import java.util.List;

public class RexUtilTest extends TestCase {

    private static final String NOTE_REX = "(//.*\\n)|(/\\*(.|\\n)*\\*/)";

    private String test = "//this is my first program;\n//i am handsome\nint main() {\n\treturn0\n}\n";

    public void testRexUtil() {
        List<String> rexTest = new RexUtil(NOTE_REX,test).getRexString();
        System.out.println(rexTest.size());
        for (int i = 0; i < rexTest.size(); i++) {
            System.out.println(rexTest.get(i));
        }
    }
}